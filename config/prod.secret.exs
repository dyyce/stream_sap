use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
#
# You should document the content of this
# file or create a script for recreating it, since it's
# kept out of version control and might be hard to recover
# or recreate for your teammates (or you later on).
config :twitchsap, Twitchsap.Endpoint,
  secret_key_base: "pceVcEqXs9Lm5tBV3rzzHQFm9xHTQdvodE3sftFiYlDjOAbcHm7qPO9byXzlWXmI"

# Configure your database
config :twitchsap, Twitchsap.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "streamsap_prod",
  password: "dsfdsf123",
  database: "streamsap_prod",
  pool_size: 20
