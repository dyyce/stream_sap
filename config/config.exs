# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :twitchsap, ecto_repos: [Twitchsap.Repo]

config :toniq, redis_url: "redis://localhost:6379/0"

config :quantum, :twitchsap,
cron: [
  # "* * * * *": fn -> (Toniq.enqueue(Twitchsap.Twitch_Api_Worker, name: "nothing")) end
  "* * * * *": {Twitchsap.Enqueue, :start}
]

config :guardian, Guardian,
  allowed_algos: ["HS512"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "Twitchsap",
  ttl: { 30, :days },
  verify_issuer: true, # optional
  secret_key: "y2LIDSBnsN1D/CvTJK5gsnRgcj3va9ZUCPaWfCFZsSWAsM+J0yDs1TbkBE9kHcMV",
  serializer: Twitchsap.GuardianSerializer,
  permissions: %{
    default: [
      :read_profile,
      :write_profile,
      :read_token,
      :revole_token
    ]
  }

# Configures the endpoint
config :twitchsap, Twitchsap.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "y2LIDSBnsN1D/CvTJK5gsnRgcj3va9ZUCPaWfCFZsSWAsM+J0yDs1TbkBE9kHcMV",
  render_errors: [view: Twitchsap.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Twitchsap.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :ueberauth, Ueberauth,
 providers: [
   github: { Ueberauth.Strategy.Github, [ opt1: "value", opts2: "value" ] }
 ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
