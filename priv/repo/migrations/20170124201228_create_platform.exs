defmodule Twitchsap.Repo.Migrations.CreatePlatform do
  use Ecto.Migration

  def change do
    create table(:platforms) do
      add :name, :string

      timestamps()
    end

  end
end
