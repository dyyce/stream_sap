defmodule Twitchsap.Repo.Migrations.CreateEvent do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :title, :string
      add :channel, :string
      add :start_time, :datetime
      add :type, :string

      timestamps()
    end

  end
end
