defmodule Twitchsap.Repo.Migrations.AlterChannels do
  use Ecto.Migration

  def change do
    alter table(:channels) do
      add :platform, :string
    end
  end
end
