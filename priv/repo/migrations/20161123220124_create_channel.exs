defmodule Twitchsap.Repo.Migrations.CreateChannel do
  use Ecto.Migration

  def change do
    create table(:channels) do
      add :name, :string
      add :language, :string
      add :stream_type, :string
      add :game, :string
      add :gender, :string

      timestamps()
    end

  end
end
