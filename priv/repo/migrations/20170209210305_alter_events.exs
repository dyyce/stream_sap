defmodule Twitchsap.Repo.Migrations.AlterEvents do
  use Ecto.Migration

  def change do
    alter table(:events) do
      add :end_time, :datetime
    end
  end
end
