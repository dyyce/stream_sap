defmodule Twitchsap.Repo.Migrations.CreateLanguage do
  use Ecto.Migration

  def change do
    create table(:languages) do
      add :language, :string

      timestamps()
    end

  end
end
