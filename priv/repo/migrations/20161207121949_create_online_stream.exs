defmodule Twitchsap.Repo.Migrations.CreateOnlineStream do
  use Ecto.Migration

  def change do
    create table(:online_streams) do
      add :name, :string
      add :game, :string
      add :image, :string
      add :viewers, :integer

      timestamps()
    end

  end
end
