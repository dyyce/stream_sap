defmodule Twitchsap.Repo.Migrations.CreateStreamtype do
  use Ecto.Migration

  def change do
    create table(:streamtypes) do
      add :stream_type, :string

      timestamps()
    end

  end
end
