defmodule Twitchsap.Repo.Migrations.AlterOnlineStreams do
  use Ecto.Migration

  def change do
    alter table(:online_streams) do
      add :language, :string
      add :stream_type, :string
      add :gender, :string
    end
  end
end
