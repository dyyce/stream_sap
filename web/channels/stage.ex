defmodule Twitchsap.StageChannel do
  use Phoenix.Channel

  alias Twitchsap.Repo
  alias Twitchsap.OnlineStream
  alias Twitchsap.OnlineStreamsQuery

  import Ecto.Query

  def join "stream:stage", _message, socket do
    {:ok, socket}
  end
end
