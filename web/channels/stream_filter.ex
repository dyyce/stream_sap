defmodule Twitchsap.StreamFilter do
  use Phoenix.Channel

  alias Twitchsap.Repo
  alias Twitchsap.OnlineStream
  alias Twitchsap.Gender
  alias Twitchsap.Language
  alias Twitchsap.Streamtype
  alias Twitchsap.OnlineStreamsQuery
  alias Twitchsap.Event

  import Ecto.Query

  intercept ["stream_changed"]

  def join "stream:main", _message, socket do
    {:ok, socket}
  end

  def join "stream:" <> _undef_stream, _params, _socket do
    {:error, %{reason: "unauthorized"}}
  end

  def handle_in "stream_changed", payload, socket do
    %{"body" => %{"stream_types" => stream_types},
      "body" => %{"languages" => languages},
      "body" => %{"genders" => genders}
    } = payload

    results = OnlineStreamsQuery.run_query(genders, languages, stream_types)
    push socket, "stream_changed", %{results: Enum.shuffle results}
    # push_results_to_client "stream_changed", results, socket

    {:noreply, socket}
  end

  def handle_in "init_streams", payload, socket do
    results = Repo.all(OnlineStream)
    |> Enum.map(fn(x) ->
      %{
        name: x.name
      }
    end)

    # creates a json object and pushed to client {results: [...]}
    # push socket, "init_streams", %{results: results}
    push_results_to_client "init_streams", results, socket
    {:noreply, socket}
  end

  def handle_in "init_stream_options", payload, socket do
    stream_types = Repo.all(Streamtype) |> Enum.map fn(x) ->
      %{x.stream_type => false}
    end
    languages = Repo.all(Language) |> Enum.map fn(x) ->
      %{x.language => false}
    end
    genders = Repo.all(Gender) |> Enum.map fn(x) ->
      %{x.gender => false}
    end

    push socket, "init_stream_options",
      %{genders: genders, languages: languages, stream_types: stream_types}

    {:noreply, socket}
  end

  def handle_in "get_events_home", payload, socket do
    events = Repo.all(Event.running_events_home)

    push socket, "get_events", %{events: events}
    {:noreply, socket}
  end

  def push_results_to_client msg, results, socket do
    push socket, msg, %{results: Enum.shuffle results}
  end
end
