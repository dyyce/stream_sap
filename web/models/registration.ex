defmodule Twitchsap.Registration do
  use Twitchsap.Web, :model

  def create(changeset, repo) do
    changeset
    |> put_change(:crypted_password, hashed_password(changeset.params["password"]))
    |> IO.inspect
    |> repo.insert
  end

  defp hashed_password(password) do
    IO.puts "***"
    IO.puts "***"
    Comeonin.Bcrypt.hashpwsalt(password)
  end
end
