defmodule Twitchsap.Streamtype do
  use Twitchsap.Web, :model

  schema "streamtypes" do
    field :stream_type, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:stream_type])
    |> validate_required([:stream_type])
  end
end
