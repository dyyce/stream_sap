defmodule Twitchsap.Language do
  use Twitchsap.Web, :model

  schema "languages" do
    field :language, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:language])
    |> validate_required([:language])
  end
end
