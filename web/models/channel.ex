defmodule Twitchsap.Channel do
  use Twitchsap.Web, :model

  schema "channels" do
    field :name, :string
    field :language, :string
    field :stream_type, :string
    field :game, :string
    field :gender, :string
    field :platform, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :language, :stream_type, :game, :gender, :platform])
    |> validate_required([:name, :language, :stream_type, :game, :gender, :platform])
    |> unique_constraint(:email)
  end
end
