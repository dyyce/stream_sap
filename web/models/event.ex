defmodule Twitchsap.Event do
  use Twitchsap.Web, :model

  import Ecto.Query
  import Ecto.Queryable

  alias Twitchsap.OnlineStream

  schema "events" do
    field :title, :string
    field :channel, :string
    field :start_time, Ecto.DateTime, default: Timex.now
    field :end_time, Ecto.DateTime, default: Timex.now
    field :type, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :channel, :start_time, :end_time, :type])
    |> validate_required([:title, :channel, :start_time, :end_time, :type])
  end

  # events
  def running_events_home do
    date = Ecto.DateTime.from_erl(:calendar.universal_time())

    query = from u in Twitchsap.Event,
      join: m in Twitchsap.OnlineStream, on: u.channel == m.name,
      select: %{title: u.title, start_time: u.start_time,
        channel: u.channel, stream_type: u.type, viewers: m.viewers},
      where: u.end_time > ^date
        # and u.start_time < u.end_time
        and u.start_time <= ^date
  end
end
