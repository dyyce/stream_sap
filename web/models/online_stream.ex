defmodule Twitchsap.OnlineStream do
  use Twitchsap.Web, :model

  schema "online_streams" do
    field :name, :string
    field :game, :string
    field :language, :string
    field :stream_type, :string
    field :gender, :string
    field :image, :string
    field :viewers, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :game, :image, :viewers])
    |> validate_required([:name])
  end
end
