// import socket from '../socket'
import * as home_types from '../constants/home_constants'

export const getStreamFilters = () => {
  return dispatch => {
    // channel.push("init_stream_options")
    dispatch(getStreamFiltersRequest())
  }
}

export const getStreamFiltersRequest = () => {
  return {
    type: home_types.GET_STREAM_FILTERS_REQUEST
  }
}

export const getStreamFiltersSuccess = (payload) => {
  return {
    type: home_types.GET_STREAM_FILTERS_SUCCESS,
    stream_filters: payload
  }
}

export const getStreamFiltersFailure = () => {
  return {
    type: home_types.GET_STREAM_FILTERS_FAILURE
  }
}

export const initOnlineStreams = () => {
  return dispatch => {
    dispatch(initOnlineStreamsRequest())
  }
}
export const initOnlineStreamsRequest = () => {
  return {
    type: home_types.INIT_ONLINE_STREAMS_REQUEST
  }
}
export const initOnlineStreamsSuccess = (streams) => {
  return {
    type: home_types.INIT_ONLINE_STREAMS_SUCCESS,
    streams
  }
}
export const initOnlineStreamsFailure = (error) => {
  return {
    type: home_types.INIT_ONLINE_STREAMS_FAILURE,
    error
  }
}

export const getOnlineStreams = (stream_options) => {
  return dispatch => {
    return dispatch(getOnlineStreamsRequest(stream_options))
  }
}

export const getOnlineStreamsRequest = (stream_options) => {
  return {
    type: home_types.GET_ONLINE_STREAMS_REQUEST,
    stream_options
  }
}

export const getOnlineStreamsSuccess = (streams) => {
  return {
    type: home_types.GET_ONLINE_STREAMS_SUCCESS,
    streams
  }
}

export const getOnlineStreamsFailure = (error) => {
  return {
    type: home_types.GET_ONLINE_STREAMS_FAILURE,
    error
  }
}

export const updateOnlineStreamList = () => {
  return dispatch => {
    dispatch(updateOnlineStreamListRequest())
  }
}

export const updateOnlineStreamListRequest = (stream_options) => {
  return {
    type: home_types.UPDATE_ONLINE_STREAM_LIST_REQUEST,
    stream_options
  }
}

export const updateOnlineStreamListSuccess = (streams) => {
  return {
    type: home_types.UPDATE_ONLINE_STREAM_LIST_SUCCESS,
    streams
  }
}

export const getEvents = () => {
  return dispatch => {
    dispatch(getEventsRequest())
  }
}

// gets only the first 4 upcoming events
export const getEventsRequest = () => {
  return {
    type: home_types.GET_EVENTS_HOME_REQUEST
  }
}

export const getEventsSuccess = (events) => {
  return {
    type: home_types.GET_EVENTS_HOME_SUCCESS,
    events
  }
}

export const getEventsFailure = (error) => {
  return {
    type: home_types.GET_EVENTS_HOME_FAILURE,
    error
  }
}
