import * as stage_types from '../constants/stage_constants'

// export const getStageStreams = (payload) => {
//   let stream_options = {
//     genders: {},
//     languages: {},
//     stream_types: {}
//   }
//
//   Object.keys(payload.genders).map((item, index) => {
//     stream_options.genders[payload.genders[index]] = false
//   })
//   Object.keys(payload.languages).map((item, index) => {
//     stream_options.languages[payload.languages[index]] = false
//   })
//   Object.keys(payload.stream_types).map((item, index) => {
//     stream_options.stream_types[payload.stream_types[index]] = false
//   })
//
//   console.log(stream_options);
//
//   // get the params from url
//   var params={};
//   window.location.search
//     .replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
//       params[key] = value.split(',');
//     }
//   );
//
//   console.log(params);
//
//   if(params.genders) {
//     Object.keys(params.genders).map((key, index) => {
//       stream_options.genders[params.genders[key].toLowerCase()] = true
//     })
//   }
//   if(params.languages) {
//     let map = {}
//     Object.keys(params.languages).map((key, index) => {
//       map[params.languages[key].toLowerCase()] = true
//       stream_options.languages[params.languages[key].toLowerCase()] = true
//     })
//   }
//   if(params.stream_types) {
//     Object.keys(params.stream_types).map((key, index) => {
//       stream_options.stream_types[params.stream_types[key].toLowerCase()] = true
//     })
//   }
//
//   return dispatch => {
//     dispatch(getStageStreamsRequest(stream_options))
//   }
// }
//
// export const getStageStreamsRequest = (stream_options) => {
//   return {
//     type: stage_types.GET_STAGE_STREAMS_REQUEST,
//     stream_options
//   }
// }
//
// export const getStageStreamsSuccess = (streams) => {
//   return {
//     type: stage_types.GET_STAGE_STREAMS_SUCCESS,
//     streams
//   }
// }
//
// export const getStageStreamsFailure = () => {
//   return {
//     type: stage_types.GET_STAGE_STREAMS_FAILURE
//   }
// }

export const playNextStream = () => {
  return {
    type: stage_types.PLAY_NEXT_STREAM
  }
}

export const playPreviousStream = () => {
  return {
    type: stage_types.PLAY_PREVIOUS_STREAM
  }
}
