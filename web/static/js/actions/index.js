import * as home_actions from './home_actions'
import * as stage_actions from './stage_actions'

export default Object.assign({}, home_actions, stage_actions)
