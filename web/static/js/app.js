import "phoenix_html"

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import createLogger from 'redux-logger'

import socket from "./socket"

import reducers from './reducers'
import actions from './actions'
import {setupRealtime} from './realtime/events'

import Home from './containers/Home'
import Stage from './containers/Stage'

require('velocity-animate');
require('velocity-animate/velocity.ui');

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true
});

let enhancer = null

if(location.hostname === 'localhost') {
  enhancer = compose(
    applyMiddleware(loggerMiddleware, thunk)
  );
} else {
  enhancer = compose(
    applyMiddleware(thunk)
  );
}

let store = createStore(reducers, enhancer)
setupRealtime(store, actions)

switch (window.location.pathname) {
  case '/':
    let home = document.getElementById("home")
    // if(home)
    ReactDOM.render(
      <Provider store={store} socket={socket}>
        <Home />
      </Provider>, home
    )
    break;
  case '/play':
    let stage = document.getElementById("stream-stage")
    // if(stage)
    ReactDOM.render(
      <Provider store={store} socket={socket}>
        <Stage />
      </Provider>, stage
    )

    break;
  default:
}
