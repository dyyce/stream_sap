import { combineReducers } from 'redux'
import home_reducer from './home_reducer'
import stage_reducer from './stage_reducer'

const reducers = combineReducers({
  home_reducer,
  stage_reducer
})

export default reducers
