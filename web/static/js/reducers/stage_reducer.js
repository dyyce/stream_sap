import _ from 'lodash'
import socket from '../socket'
import * as stage_types from '../constants/stage_constants'
import * as home_types from '../constants/home_constants'

socket.connect()
let channel = socket.channel("stream:main", {})

const INIT_STATE = {
  streams: [],
  current_stream: null,
  current_stream_counter: 0,
  loading: false,
  stream_filters: {
    languages: [],
    genders: [],
    stream_types: []
  },
}

const stage_reducer = (state = INIT_STATE, action) => {
  switch(action.type) {
    case home_types.GET_STREAM_FILTERS_SUCCESS:
      let stream_options = {
        genders: {},
        languages: {},
        stream_types: {}
      }

      action.stream_filters.genders.map((item, index) => {
        stream_options.genders[Object.keys(item)[0]] = false
      })
      action.stream_filters.languages.map((item, index) => {
        stream_options.languages[Object.keys(item)[0]] = false
      })
      action.stream_filters.stream_types.map((item, index) => {
        stream_options.stream_types[Object.keys(item)[0]] = false
      })

      // get the params from url
      var params = {};
      window.location.search
        .replace(/[?&]+([^=&]+)=([^&]*)/gi, function(str,key,value) {
          params[key] = value.split(',');
        }
      );

      if(params.genders) {
        Object.keys(params.genders).map((key, index) => {
          stream_options.genders[params.genders[key].toLowerCase()] = true
        })
      }
      if(params.languages) {
        let map = {}
        Object.keys(params.languages).map((key, index) => {
          map[params.languages[key].toLowerCase()] = true
          stream_options.languages[params.languages[key].toLowerCase()] = true
        })
      }
      if(params.stream_types) {
        Object.keys(params.stream_types).map((key, index) => {
          stream_options.stream_types[params.stream_types[key].toLowerCase()] = true
        })
      }
      if(params.channel) {
        return Object.assign({}, state, {
          streams: params.channel,
          current_stream: params.channel[0],
          loading: false
        })
      }

      channel = socket.channel("stream:main", {})
      channel.join()
      channel.push('stream_changed', { body: stream_options })

      return Object.assign({}, state, {
        stream_filters: action.stream_filters,
        loading: false,
      })

    case home_types.GET_ONLINE_STREAMS_REQUEST:
      return Object.assign({}, state, {
        loading: true
      })
    case home_types.GET_ONLINE_STREAMS_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        streams: action.streams,
        current_stream: _.includes(action.streams, state.current_stream)
          ? state.current_stream
          : action.streams[0]
      })
    case home_types.GET_ONLINE_STREAMS_FAILURE:
      return Object.assign({}, state, {
        loading: false
      })
    case stage_types.PLAY_NEXT_STREAM:
      let current_stream_counter = state.current_stream_counter + 1 < state.streams.length
        ? state.current_stream_counter + 1
        : state.current_stream_counter

      return Object.assign({}, state, {
        current_stream_counter: current_stream_counter,
        current_stream: state.streams[current_stream_counter]
      })
    case stage_types.PLAY_PREVIOUS_STREAM:
      current_stream_counter = state.current_stream_counter > 0
        ? state.current_stream_counter - 1
        : state.current_stream_counter

      return Object.assign({}, state, {
        current_stream_counter: current_stream_counter,
        current_stream: state.streams[current_stream_counter]
      })

    default:
      return state
  }
}

export default stage_reducer
