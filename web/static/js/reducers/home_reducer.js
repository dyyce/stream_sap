import * as home_types from '../constants/home_constants'
import socket from '../socket'

socket.connect()
let channel = socket.channel("stream:main", {})

const INIT_STATE = {
  streams: [],
  stream_filters: {
    languages: [],
    genders: [],
    stream_types: []
  },
  loading: false,
  events: []
}

const home_reducer = (state = INIT_STATE, action) => {
  switch(action.type) {
    case home_types.GET_STREAM_FILTERS_REQUEST:
      channel = socket.channel("stream:main", {})
      channel.join()
      channel.push('init_stream_options')

      return Object.assign({}, state, {
        loading: true
      })
    case home_types.GET_STREAM_FILTERS_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        stream_filters: action.stream_filters,
      })
    case home_types.INIT_ONLINE_STREAMS_REQUEST:
      channel = socket.channel("stream:main", {})
      channel.join()
      channel.push('init_streams')

      return Object.assign({}, state, {
        loading: true
      })
    case home_types.INIT_ONLINE_STREAMS_SUCCESS:
      return Object.assign({}, state, {
        streams: action.streams,
        loading: false
      })
    case home_types.GET_ONLINE_STREAMS_REQUEST:
      channel = socket.channel("stream:main", {})
      channel.join()
      channel.push('stream_changed', { body: action.stream_options })

      return Object.assign({}, state, {
        loading: true
      })
    case home_types.GET_ONLINE_STREAMS_SUCCESS:
      // keep timeout instead of interval
      setTimeout(() => {
        let stream_options = {
          genders: {},
          languages: {},
          stream_types: {}
        }

        state.stream_filters.genders.map((item, index) => {
          stream_options.genders[Object.keys(item)[0]] = false
        })
        state.stream_filters.languages.map((item, index) => {
          stream_options.languages[Object.keys(item)[0]] = false
        })
        state.stream_filters.stream_types.map((item, index) => {
          stream_options.stream_types[Object.keys(item)[0]] = false
        })
        // console.log('updating online streams list', stream_options);
        channel = socket.channel("stream:main", {})
        channel.join()
        channel.push('stream_changed', { body: stream_options })
      }, 60000) // 60s update interval

      return Object.assign({}, state, {
        loading: false,
        streams: action.streams
      })
    case home_types.GET_EVENTS_HOME_REQUEST:
      channel = socket.channel('stream:main', {})
      channel.join()
      channel.push('get_events_home', {})

      return Object.assign({}, state, {
        loading: true
      })
    case home_types.GET_EVENTS_HOME_SUCCESS:
      return Object.assign({}, state, {
        loading: false,
        events: action.events
      })

    default:
      return state
  }
}

export default home_reducer
