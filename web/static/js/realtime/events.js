import socket from '../socket'

// init data comes from socket.js
export const setupRealtime = (store, actions) => {
  // let state = store.getState()

  let channel = socket.channel("stream:main", {})

  // realtime events
  channel.on("init_streams", payload => {
    store.dispatch(actions.initOnlineStreamsSuccess(payload.results))
  })

  channel.on("init_stream_options", payload => {
    store.dispatch(actions.getStreamFiltersSuccess(payload))
  })

  channel.on("stream_changed", payload => {
    store.dispatch(actions.getOnlineStreamsSuccess(payload.results))
  })

  channel.on('get_events', payload => {
    store.dispatch(actions.getEventsSuccess(payload.events))
  })
}
