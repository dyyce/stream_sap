import Stage from './Stage'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as stage_actions from '../../actions/stage_actions'
import * as home_actions from '../../actions/home_actions'

export default connect(
  state => {
    return ({
      // props here
      streams: state.stage_reducer.streams,
      current_stream: state.stage_reducer.current_stream,
      current_stream_counter: state.stage_reducer.current_stream_counter
    })
  },
  dispatch => bindActionCreators(Object.assign({}, home_actions, stage_actions), dispatch)
)(Stage)
