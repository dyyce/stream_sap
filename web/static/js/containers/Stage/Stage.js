import React, {Component} from 'react'

class Stage extends Component {
  componentWillMount() {
    this.state = {
      streams: [],
      current_stream: null,
      current_streamCounter: 0
    }
  }

  componentDidMount() {
    this.props.getStreamFilters()
  }

  _playNextStream(e) {
    this.props.playNextStream()
    this.setState({
      current_streamCounter: this.state.current_streamCounter + 1 < this.state.streams.length
        ? this.state.current_streamCounter + 1
        : this.state.current_streamCounter
    }, () => {

      this.setState({
        current_stream: this.state.streams[this.state.current_streamCounter]
      })
    })
  }

  _playPreviousStream(e) {
    this.props.playPreviousStream()
    this.setState({
      current_streamCounter: this.state.current_streamCounter > 0
        ? this.state.current_streamCounter - 1
        : this.state.current_streamCounter
    }, () => {
      this.setState({
        current_stream: this.state.streams[this.state.current_streamCounter]
      })
    })
  }

  renderStream() {
    return this.props.current_stream
      ?  <div>
        <button
          className="btn btn-gray"
          onClick={() => document.location.href = '/'}>
          <i className="fa fa-arrow-left"></i>
        </button>

        <iframe
          style={{
            width: '100%',
            height: '80vh'
          }}
          src={`http://player.twitch.tv/?channel=${this.props.current_stream}&autoplay&!muted`}
          className="frame" allowFullScreen>
        </iframe>

        {this.props.streams.length > 1
          ? <div className="btn-group" role="group" aria-label="Basic example">
            <button
              className="btn btn-gray"
              disabled={this.props.current_stream_counter === 0}
              onClick={this._playPreviousStream.bind(this)}>
              <i className="fa fa-step-backward"></i>
            </button>
            <button
              className="btn btn-gray"
              disabled={this.props.current_stream_counter === this.props.streams.length - 1}
              onClick={this._playNextStream.bind(this)}>
              <i className="fa fa-step-forward"></i>
            </button>
          </div>
          : <div></div>
        }
      </div>
      :<div id="spinner-wrap">
        <div className="sk-folding-cube">
          <div className="sk-cube1 sk-cube"></div>
          <div className="sk-cube2 sk-cube"></div>
          <div className="sk-cube4 sk-cube"></div>
          <div className="sk-cube3 sk-cube"></div>
        </div>
      </div>
  }

  render() {
    return <div>
      {this.renderStream()}
    </div>
  }
}

export default Stage
