import React, {Component} from 'react'
import _ from 'lodash'
import OwlCarousel from 'react-owl-carousel'

class Home extends Component {

  componentWillMount() {
    this.state = {
      itemsCount: 40,
      playBtnTxt: `Play`,
      stream_options: {
        languages: {},
        stream_types: {},
        genders: {}
      }
    }
  }

  componentDidMount() {
    this.props.getStreamFilters()
    this.props.initOnlineStreams()
    this.props.getEvents()
  }

  _streamFilterChanged() {
    this.props.getOnlineStreams(this.state.stream_options)
  }

  _playStreams() {
    let play_langs = Object.keys(this.state.stream_options.languages).map((key, idx) => {
      if(this.state.stream_options.languages[key]) {
        return key
      }
      else {
        return null
      }
    }).filter(n => n)

    let play_genders = Object.keys(this.state.stream_options.genders).map((key, idx) => {
      if(this.state.stream_options.genders[key]) {
        return key
      }
      else {
        return null
      }
    }).filter(n => n)

    let play_stream_types = Object.keys(this.state.stream_options.stream_types).map((key, idx) => {
      if(this.state.stream_options.stream_types[key]) {
        return key
      }
      else {
        return null
      }
    }).filter(n => n)

    let lang_query_string = play_langs.length > 0 ? `&languages=${play_langs}` : ''
    let gender_query_string = play_genders.length > 0 ? `&genders=${play_genders}` : ''
    let stream_type_query_string = play_stream_types.length > 0 ? `&stream_types=${play_stream_types}` : ''

    document.location.href = `${document.location.href}play?${lang_query_string}${gender_query_string}${stream_type_query_string}`
  }

  renderStreamFilters() {
    let view = this.props.stream_filters
      ? <div className="row mb20">
        <div id="languages" className="col-sm-12 filter-group mb20">
          <div className="btn-group col-sm-6 col-sm-offset-4" data-toggle="buttons">
            {this.props.stream_filters.languages.map((item, key) => {
              if(this.state.stream_options.languages[Object.keys(item)[0]] === undefined)
                this.state.stream_options.languages[Object.keys(item)[0]] = false
              return <label
                key={key}
                onClick={() => {
                  this.state.stream_options.languages[Object.keys(item)[0]] = !this.state.stream_options.languages[Object.keys(item)[0]]

                  this.setState({
                    stream_options: this.state.stream_options
                  }, () => {
                    this._streamFilterChanged()
                  })
                }}
                type="checkbox" className={`btn btn-lg btn-cerulean btn-outline ${this.state.stream_options.languages[Object.keys(item)[0]]
                  ? 'active'
                  : ''}`}>
                {_.startCase(Object.keys(item)[0])}
              </label>
            })}
          </div>
        </div>

        <div id="genders" className="col-sm-12 filter-group mb20">
          <div className="btn-group col-sm-6 col-sm-offset-4" data-toggle="buttons">
            {this.props.stream_filters.genders.map((item, key) => {
              if(this.state.stream_options.genders[Object.keys(item)[0]] === undefined)
                this.state.stream_options.genders[Object.keys(item)[0]] = false
              return <label
                key={key}
                onClick={() => {
                  this.state.stream_options.genders[Object.keys(item)[0]] = !this.state.stream_options.genders[Object.keys(item)[0]]
                  this.setState({
                    stream_options: this.state.stream_options
                  }, () => {
                    this._streamFilterChanged()
                  })
                }}
                type="button" className={`btn btn-lg btn-cerulean btn-outline ${this.state.stream_options.genders[Object.keys(item)[0]]
                  ? 'active'
                  : ''}`}>
                {_.startCase(Object.keys(item)[0])}
              </label>
            })}
          </div>
        </div>

        <div id="stream-types" className="col-sm-12 filter-group mb20">
          <div className="btn-group col-sm-6 col-sm-offset-4" data-toggle="buttons">
            {this.props.stream_filters.stream_types.map((item, key) => {
              if(this.state.stream_options.stream_types[Object.keys(item)[0]] === undefined)
                this.state.stream_options.stream_types[Object.keys(item)[0]] = false
              return <label
                key={key}
                onClick={() => {
                  this.state.stream_options.stream_types[Object.keys(item)[0]] = !this.state.stream_options.stream_types[Object.keys(item)[0]]
                  this.setState({
                    stream_options: this.state.stream_options
                  }, () => {
                    this._streamFilterChanged()
                  })
                }}
                type="button" className={`btn btn-lg btn-cerulean btn-outline ${this.state.stream_options.stream_types[Object.keys(item)[0]]
                  ? 'active'
                  : ''}`}>
                {_.startCase(Object.keys(item)[0])}
              </label>
            })}
          </div>
        </div>
      </div>

      : <div className="row mb20">
        <div className="col-sm-12 filter-group mb20">
          <div className="col-sm-offset-4">
            loading...
          </div>
        </div>
      </div>

    return view;
  }

  renderEvents() {
    let events = this.props.events.length > 0
      ? this.props.events.map((event, key) => {
        return <div className="panel panel-default event-panel" key={key}
          onClick={() => {
            let event_link = `http://${document.location.host}/play?channel=${event.channel}`
            document.location.href = event_link
          }}>
          <div className="panel-heading">
            <h3 className="panel-title">
              <span className="m-r-1 event-title">{event.channel}</span> <span className="label label-primary label-outline small">{event.stream_type}</span>
            </h3>
          </div>
          <div className="panel-body p-3">
            <h4>
              {event.title}
            </h4>
            <div className="">
              <span className="event-viewers-label label label-outline label-danger pull-right">{event.viewers}</span>
            </div>
          </div>
        </div>
      })
      : <div>no events at the moment</div>

    return <OwlCarousel slideSpeed={300} autoPlay
      items={4}
      >
      {events}
    </OwlCarousel>
  }

  renderPlayBtn() {
    let playBtn = <div className="row">
      <div className="col-sm-12">
        <div className="col-sm-4 col-sm-offset-4">
          <button
            onClick={this._playStreams.bind(this)}
            disabled={this.props.loading}
            id="front-play-btn" className="btn btn-lg btn-primary btn-block animated bounce">
            {this.props.loading
              ? 'Checking for online streams...'
              : this.props.streams.length > 0 ? `Play ${this.props.streams.length} streams` : 'No streams available'}
          </button>
        </div>
      </div>
    </div>
    return playBtn
  }

  render() {
    let home = <div style={{ position: 'relative', height: '100%' }}>

      <div className="mb20"></div>

      <div className="row front-page-events">
        {this.renderEvents()}
      </div>

      <div className="mb20"></div>

      <div className="container front-page" id="front-page">
        {this.renderStreamFilters()}
        {this.renderPlayBtn()}
      </div>

      {/* <i className="fa fa-chevron-down fa-stack-2x fa-inverse" id="arrow"
        style={{ bottom: 0, position: 'absolute' }}>
      </i> */}
    </div>

    return home
  }
}

export default Home
