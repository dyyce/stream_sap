import Home from './Home'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../../actions/home_actions'

export default connect(
  state => {
    return ({
      stream_filters: state.home_reducer.stream_filters,
      streams: state.home_reducer.streams,
      loading: state.home_reducer.loading,
      events: state.home_reducer.events
      // socket: this.props.socket
    })
  },
  dispatch => bindActionCreators(actions, dispatch)
)(Home)
