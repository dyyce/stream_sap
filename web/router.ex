defmodule Twitchsap.Router do
  use Twitchsap.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Twitchsap do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/privacy", PageController, :privacy
    get "/play", PageController, :play
    get "/play/:id", PageController, :single_channel_play

    get    "/login",  SessionController, :new
    post   "/login",  SessionController, :create
    delete "/logout", SessionController, :delete

    resources "/registrations", RegistrationController, only: [:new, :create]
    resources "/events", EventController
    resources "/channels", ChannelController
    resources "/languages", LanguageController
    resources "/games", GameController
    resources "/streamtypes", StreamtypeController
    resources "/genders", GenderController
    resources "/platforms", PlatformController
  end

  # Other scopes may use custom stacks.
  # scope "/api", Twitchsap do
  #   pipe_through :api
  # end
end
