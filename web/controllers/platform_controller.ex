defmodule Twitchsap.PlatformController do
  use Twitchsap.Web, :controller

  alias Twitchsap.Platform
  plug Twitchsap.Plugs.RequireAdmin

  def index(conn, _params) do
    platforms = Repo.all(Platform)
    render(conn, "index.html", platforms: platforms)
  end

  def new(conn, _params) do
    changeset = Platform.changeset(%Platform{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"platform" => platform_params}) do
    changeset = Platform.changeset(%Platform{}, platform_params)

    case Repo.insert(changeset) do
      {:ok, _platform} ->
        conn
        |> put_flash(:info, "Platform created successfully.")
        |> redirect(to: platform_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    platform = Repo.get!(Platform, id)
    render(conn, "show.html", platform: platform)
  end

  def edit(conn, %{"id" => id}) do
    platform = Repo.get!(Platform, id)
    changeset = Platform.changeset(platform)
    render(conn, "edit.html", platform: platform, changeset: changeset)
  end

  def update(conn, %{"id" => id, "platform" => platform_params}) do
    platform = Repo.get!(Platform, id)
    changeset = Platform.changeset(platform, platform_params)

    case Repo.update(changeset) do
      {:ok, platform} ->
        conn
        |> put_flash(:info, "Platform updated successfully.")
        |> redirect(to: platform_path(conn, :show, platform))
      {:error, changeset} ->
        render(conn, "edit.html", platform: platform, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    platform = Repo.get!(Platform, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(platform)

    conn
    |> put_flash(:info, "Platform deleted successfully.")
    |> redirect(to: platform_path(conn, :index))
  end
end
