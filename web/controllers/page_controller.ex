defmodule Twitchsap.PageController do
  use Twitchsap.Web, :controller

  alias Twitchsap.Repo

  alias Twitchsap.OnlineStreamsQuery

  alias Twitchsap.Channel
  alias Twitchsap.Streamtype
  alias Twitchsap.Gender
  alias Twitchsap.Language
  alias Twitchsap.OnlineStream
  alias Twitchsap.User

  def index(conn, _params) do
    stream_types = Repo.all(Streamtype)
    genders = Repo.all(Gender)
    streams = Repo.all(Channel)
    languages = Repo.all(Language)

    render conn, "index.html",
      stream_types: stream_types, genders: genders,
      streams: streams, languages: languages
  end

  def privacy(conn, _params) do

    render conn, "privacy.html"
  end

  def play(conn, params) do
    genders = cond do
      params["genders"] ->
        String.split params["genders"], ","
      params["genders"] == nil ->
        Repo.all(Gender) |> Enum.map(fn(x) -> x.gender end)
    end

    languages = cond do
      params["languages"] ->
        String.split params["languages"], ","
      params["languages"] == nil ->
        Repo.all(Language) |> Enum.map(fn(x) -> x.language end)
    end

    stream_types = cond do
      params["stream_types"] ->
        String.split params["stream_types"], ","
      params["stream_types"] == nil ->
        Repo.all(Streamtype) |> Enum.map(fn(x) -> x.stream_type end)
    end

    genders_map = %{}
    genders_map = Enum.reduce genders, %{}, fn x, acc ->
      Map.put(acc, x, true)
    end

    languages_map = %{}
    languages_map = Enum.reduce languages, %{}, fn x, acc ->
      Map.put(acc, x, true)
    end

    stream_types_map = %{}
    stream_types_map = Enum.reduce stream_types, %{}, fn x, acc ->
      Map.put(acc, x, true)
    end

    results = OnlineStreamsQuery.run_query(genders_map, languages_map, stream_types_map)
    render conn, "playstream.html", results: %{"results" => results}
  end

  def single_channel_play conn, params do
    render conn, "playstream.html", results: %{"results" => ["dotastarladder_en"]}
  end

  def find_and_confirm_password(user) do
    changeset = User.changeset(%User{}, user)
  end
end
