defmodule Twitchsap.Plugs.RequireAdmin do
  import Plug.Conn
  import Phoenix.Controller
  import Ecto.Query

  alias Twitchsap.Repo
  alias Twitchsap.User

  def init(_params) do
  end

  def call(conn, _params) do
    if get_session(conn, :current_user) do
      current_user = Repo.get_by(User, id: get_session(conn, :current_user))

      if current_user.admin do
        conn
      else
        conn
        |> put_flash(:error, "No access for u here.")
        |> redirect(to: "/")
        |> halt()
      end
    else
      conn
      |> put_flash(:error, "You are not logged in.")
      |> redirect(to: "/login")
      |> halt()
    end
  end
end
