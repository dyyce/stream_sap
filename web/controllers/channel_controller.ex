defmodule Twitchsap.ChannelController do
  use Twitchsap.Web, :controller

  alias Twitchsap.Repo
  alias Twitchsap.Scripts

  alias Twitchsap.Channel
  alias Twitchsap.Language
  alias Twitchsap.Streamtype
  alias Twitchsap.Game
  alias Twitchsap.Gender
  alias Twitchsap.Platform

  plug Twitchsap.Plugs.RequireAdmin

  def index(conn, _params) do
    channels = Repo.all(Channel)
    render(conn, "index.html", channels: channels)
  end

  def new(conn, _params) do
    changeset = Channel.changeset(%Channel{})

    languages = Repo.all(Language)
    formatted_languages = Enum.map(languages, fn(lang) ->
      %{language: language} = lang
      language
    end)

    streamtypes = Repo.all(Streamtype)
    formatted_streamtypes = Enum.map(streamtypes, fn(type) ->
      %{stream_type: streamtype} = type
      streamtype
    end)

    games = Repo.all(Game)
    formatted_games = Enum.map(games, fn(game) ->
      %{name: game_name} = game
      game_name
    end)

    genders = Repo.all(Gender)
    formatted_genders = Enum.map(genders, fn(gender) ->
      %{gender: type} = gender
      type
    end)

    platforms = Repo.all(Platform)
    formatted_platforms = Enum.map(platforms, fn(platform) ->
      %{name: type} = platform
      type
    end)

    render conn, "new.html", changeset: changeset,
      languages: formatted_languages, streamtypes: formatted_streamtypes, games: formatted_games, genders: formatted_genders,
      platforms: formatted_platforms
  end

  def create(conn, %{"channel" => channel_params}) do
    changeset = Channel.changeset(%Channel{}, channel_params)

    case Repo.get_by(Channel, name: Map.get(channel_params, "name")) do
      nil ->
        conn
      channel ->
        # IO.inspect channel
        conn
        |> put_flash(:error, "Channel '" <> channel.name <> "' already exists.")
        |> redirect(to: channel_path(conn, :index))
    end

    languages = Repo.all(Language)
    formatted_languages = Enum.map(languages, fn(lang) ->
      %{language: language} = lang
      language
    end)

    streamtypes = Repo.all(Streamtype)
    formatted_streamtypes = Enum.map(streamtypes, fn(type) ->
      %{stream_type: streamtype} = type
      streamtype
    end)

    games = Repo.all(Game)
    formatted_games = Enum.map(games, fn(game) ->
      %{name: game_name} = game
      game_name
    end)

    genders = Repo.all(Gender)
    formatted_genders = Enum.map(genders, fn(gender) ->
      %{gender: type} = gender
      type
    end)

    platforms = Repo.all(Platform)
    formatted_platforms = Enum.map(platforms, fn(platform) ->
      %{name: type} = platform
      type
    end)

    case Repo.insert(changeset) do
      {:ok, _channel} ->
        conn
        |> put_flash(:info, "Channel added successfully.")
        |> redirect(to: channel_path(conn, :index))
      {:error, changeset} ->
        render conn, "new.html", changeset: changeset,
          languages: formatted_languages, streamtypes: formatted_streamtypes, games: formatted_games, genders: formatted_genders,
          platforms: formatted_platforms
    end
  end

  def show(conn, %{"id" => id}) do
    channel = Repo.get!(Channel, id)
    render(conn, "show.html", channel: channel)
  end

  def edit(conn, %{"id" => id}) do
    channel = Repo.get!(Channel, id)
    changeset = Channel.changeset(channel)

    languages = Repo.all(Language)
    formatted_languages = Enum.map(languages, fn(lang) ->
      %{language: language} = lang
      language
    end)

    streamtypes = Repo.all(Streamtype)
    formatted_streamtypes = Enum.map(streamtypes, fn(type) ->
      %{stream_type: streamtype} = type
      streamtype
    end)

    games = Repo.all(Game)
    formatted_games = Enum.map(games, fn(game) ->
      %{name: game_name} = game
      game_name
    end)

    genders = Repo.all(Gender)
    formatted_genders = Enum.map(genders, fn(gender) ->
      %{gender: type} = gender
      type
    end)

    platforms = Repo.all(Platform)
    formatted_platforms = Enum.map(platforms, fn(platform) ->
      %{name: type} = platform
      type
    end)

    render conn, "edit.html", channel: channel, changeset: changeset,
      languages: formatted_languages, streamtypes: formatted_streamtypes, games: formatted_games, genders: formatted_genders,
      platforms: formatted_platforms
  end

  def update(conn, %{"id" => id, "channel" => channel_params}) do
    channel = Repo.get!(Channel, id)
    changeset = Channel.changeset(channel, channel_params)

    languages = Repo.all(Language)
    formatted_languages = Enum.map(languages, fn(lang) ->
      %{language: language} = lang
      language
    end)

    streamtypes = Repo.all(Streamtype)
    formatted_streamtypes = Enum.map(streamtypes, fn(type) ->
      %{stream_type: streamtype} = type
      streamtype
    end)

    games = Repo.all(Game)
    formatted_games = Enum.map(games, fn(game) ->
      %{name: game_name} = game
      game_name
    end)

    genders = Repo.all(Gender)
    formatted_genders = Enum.map(genders, fn(gender) ->
      %{gender: type} = gender
      type
    end)

    platforms = Repo.all(Platform)
    formatted_platforms = Enum.map(platforms, fn(platform) ->
      %{name: type} = platform
      type
    end)

    case Repo.update(changeset) do
      {:ok, channel} ->
        conn
        |> put_flash(:info, "Channel updated successfully.")
        |> redirect(to: channel_path(conn, :show, channel))
      {:error, changeset} ->
        render conn, "edit.html", channel: channel, changeset: changeset,
          languages: formatted_languages, streamtypes: formatted_streamtypes, games: formatted_games, genders: formatted_genders,
          platforms: formatted_platforms
    end
  end

  def delete(conn, %{"id" => id}) do
    channel = Repo.get!(Channel, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(channel)

    conn
    |> put_flash(:info, "Channel deleted successfully.")
    |> redirect(to: channel_path(conn, :index))
  end
end
