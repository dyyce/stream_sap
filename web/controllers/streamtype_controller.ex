defmodule Twitchsap.StreamtypeController do
  use Twitchsap.Web, :controller

  alias Twitchsap.Streamtype
  plug Twitchsap.Plugs.RequireAdmin

  def index(conn, _params) do
    streamtypes = Repo.all(Streamtype)
    render(conn, "index.html", streamtypes: streamtypes)
  end

  def new(conn, _params) do
    changeset = Streamtype.changeset(%Streamtype{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"streamtype" => streamtype_params}) do
    changeset = Streamtype.changeset(%Streamtype{}, streamtype_params)

    case Repo.insert(changeset) do
      {:ok, _streamtype} ->
        conn
        |> put_flash(:info, "Streamtype created successfully.")
        |> redirect(to: streamtype_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    streamtype = Repo.get!(Streamtype, id)
    render(conn, "show.html", streamtype: streamtype)
  end

  def edit(conn, %{"id" => id}) do
    streamtype = Repo.get!(Streamtype, id)
    changeset = Streamtype.changeset(streamtype)
    render(conn, "edit.html", streamtype: streamtype, changeset: changeset)
  end

  def update(conn, %{"id" => id, "streamtype" => streamtype_params}) do
    streamtype = Repo.get!(Streamtype, id)
    changeset = Streamtype.changeset(streamtype, streamtype_params)

    case Repo.update(changeset) do
      {:ok, streamtype} ->
        conn
        |> put_flash(:info, "Streamtype updated successfully.")
        |> redirect(to: streamtype_path(conn, :show, streamtype))
      {:error, changeset} ->
        render(conn, "edit.html", streamtype: streamtype, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    streamtype = Repo.get!(Streamtype, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(streamtype)

    conn
    |> put_flash(:info, "Streamtype deleted successfully.")
    |> redirect(to: streamtype_path(conn, :index))
  end
end
