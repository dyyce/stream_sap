defmodule Twitchsap.Scripts do
  alias Twitchsap.Repo
  alias Twitchsap.Channel

  import Ecto.Query

  def set_to_twitch do
    # https://hexdocs.pm/ecto/2.0.0-rc.5/Ecto.Repo.html#c:update_all/3
    Repo.update_all(Channel, set: [platform: "twitch"])
  end
end
