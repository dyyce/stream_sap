defmodule Twitchsap.Twitch_Api_Worker do
  use Toniq.Worker

  alias Twitchsap.Repo
  alias Twitchsap.Channel
  alias Twitchsap.OnlineStream

  def perform(name: name) do

    # check_for_online_streams
    IO.puts "Just woke up. Ready for work 💪"
    IO.puts "checking for online streams 💻"

    # go through stream list and check which are online
    online_streams = Repo.all(Channel)
    |> Enum.map(fn(x) ->
      # check
      result = HTTPotion.get "https://api.twitch.tv/kraken/streams/" <> x.name,
        [headers: ["Client-ID": "1q8dv4d45lcczr8m590gnwb07zv9wsa"]]
      %{"stream" => stream} = Poison.decode! result.body

      cond do
        stream == nil ->
          nil
        true ->
          # %{"display_name" => name} = stream["channel"]

          %OnlineStream{
            name: x.name,
            gender: x.gender,
            game: x.game,
            stream_type: x.stream_type,
            language: x.language,
            viewers: stream["viewers"]
          }
      end
    end)
    |> Enum.filter(fn(x) -> x != nil end)

    IO.puts "clearing old online_streams 🗑"
    Repo.delete_all(OnlineStream)

    IO.puts "online streams found 📺:"
    IO.inspect online_streams

    IO.puts "inserting fresh onlne streams ✍️"
    online_streams
    |> Enum.map(fn(x) -> Repo.insert(x) end)

    :timer.sleep 2000
    IO.puts "job finished 👍"
    IO.puts "doing a power nap 😴"
  end
end
