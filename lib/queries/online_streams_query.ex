defmodule Twitchsap.OnlineStreamsQuery do
    alias Twitchsap.Repo
    alias Twitchsap.OnlineStream

    import Ecto.Query

  def run_query genders, languages, stream_types do
    # IO.puts "***"
    # IO.inspect genders
    # IO.inspect languages
    # IO.inspect stream_types
    # IO.puts "***"

    query = from c in OnlineStream,
      where: c.gender in ^get_filtered_results_query(genders),
      where: c.language in ^get_filtered_results_query(languages),
      where: c.stream_type in ^get_filtered_results_query(stream_types)

    results = Repo.all(query)
    |> Enum.map(fn(x) ->
      x
    end)
    |> filter_for_online_stream
  end

  def get_filtered_results_query channels do
    channels_filtered = Enum.filter(channels, fn(x) ->
      {_lul, this_is_true } = x
      this_is_true
    end)
    channels_for_query = case Enum.count(channels_filtered) == 0 do
      true -> Enum.map(channels, fn(x) ->
        {chann, _tail} = x
        chann
      end)
      false -> Enum.map(channels_filtered, fn(x) ->
        {chann, _tail} = x
        chann
      end)
    end
  end

  def filter_for_online_stream channels do
    online_streams = Enum.map(channels, fn(x) ->
      result = HTTPotion.get "https://api.twitch.tv/kraken/streams/" <> x.name,
        [headers: ["Client-ID": "1q8dv4d45lcczr8m590gnwb07zv9wsa"]]
      %{"stream" => stream} = Poison.decode! result.body

      cond do
        stream == nil ->
          nil
        true ->
          %{"display_name" => name} = stream["channel"]
          x.name
      end
    end)
    |> Enum.filter(fn(x) -> x != nil end)

    IO.inspect online_streams
    IO.puts "Online streams " <> to_string Enum.count online_streams

    online_streams
  end
end
