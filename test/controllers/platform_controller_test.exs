defmodule Twitchsap.PlatformControllerTest do
  use Twitchsap.ConnCase

  alias Twitchsap.Platform
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, platform_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing platforms"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, platform_path(conn, :new)
    assert html_response(conn, 200) =~ "New platform"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, platform_path(conn, :create), platform: @valid_attrs
    assert redirected_to(conn) == platform_path(conn, :index)
    assert Repo.get_by(Platform, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, platform_path(conn, :create), platform: @invalid_attrs
    assert html_response(conn, 200) =~ "New platform"
  end

  test "shows chosen resource", %{conn: conn} do
    platform = Repo.insert! %Platform{}
    conn = get conn, platform_path(conn, :show, platform)
    assert html_response(conn, 200) =~ "Show platform"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, platform_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    platform = Repo.insert! %Platform{}
    conn = get conn, platform_path(conn, :edit, platform)
    assert html_response(conn, 200) =~ "Edit platform"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    platform = Repo.insert! %Platform{}
    conn = put conn, platform_path(conn, :update, platform), platform: @valid_attrs
    assert redirected_to(conn) == platform_path(conn, :show, platform)
    assert Repo.get_by(Platform, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    platform = Repo.insert! %Platform{}
    conn = put conn, platform_path(conn, :update, platform), platform: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit platform"
  end

  test "deletes chosen resource", %{conn: conn} do
    platform = Repo.insert! %Platform{}
    conn = delete conn, platform_path(conn, :delete, platform)
    assert redirected_to(conn) == platform_path(conn, :index)
    refute Repo.get(Platform, platform.id)
  end
end
