defmodule Twitchsap.StreamtypeControllerTest do
  use Twitchsap.ConnCase

  alias Twitchsap.Streamtype
  @valid_attrs %{stream_type: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, streamtype_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing streamtypes"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, streamtype_path(conn, :new)
    assert html_response(conn, 200) =~ "New streamtype"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, streamtype_path(conn, :create), streamtype: @valid_attrs
    assert redirected_to(conn) == streamtype_path(conn, :index)
    assert Repo.get_by(Streamtype, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, streamtype_path(conn, :create), streamtype: @invalid_attrs
    assert html_response(conn, 200) =~ "New streamtype"
  end

  test "shows chosen resource", %{conn: conn} do
    streamtype = Repo.insert! %Streamtype{}
    conn = get conn, streamtype_path(conn, :show, streamtype)
    assert html_response(conn, 200) =~ "Show streamtype"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, streamtype_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    streamtype = Repo.insert! %Streamtype{}
    conn = get conn, streamtype_path(conn, :edit, streamtype)
    assert html_response(conn, 200) =~ "Edit streamtype"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    streamtype = Repo.insert! %Streamtype{}
    conn = put conn, streamtype_path(conn, :update, streamtype), streamtype: @valid_attrs
    assert redirected_to(conn) == streamtype_path(conn, :show, streamtype)
    assert Repo.get_by(Streamtype, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    streamtype = Repo.insert! %Streamtype{}
    conn = put conn, streamtype_path(conn, :update, streamtype), streamtype: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit streamtype"
  end

  test "deletes chosen resource", %{conn: conn} do
    streamtype = Repo.insert! %Streamtype{}
    conn = delete conn, streamtype_path(conn, :delete, streamtype)
    assert redirected_to(conn) == streamtype_path(conn, :index)
    refute Repo.get(Streamtype, streamtype.id)
  end
end
