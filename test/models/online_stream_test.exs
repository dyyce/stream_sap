defmodule Twitchsap.OnlineStreamTest do
  use Twitchsap.ModelCase

  alias Twitchsap.OnlineStream

  @valid_attrs %{game: "some content", image: "some content", name: "some content", viewers: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = OnlineStream.changeset(%OnlineStream{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = OnlineStream.changeset(%OnlineStream{}, @invalid_attrs)
    refute changeset.valid?
  end
end
