defmodule Twitchsap.StreamtypeTest do
  use Twitchsap.ModelCase

  alias Twitchsap.Streamtype

  @valid_attrs %{stream_type: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Streamtype.changeset(%Streamtype{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Streamtype.changeset(%Streamtype{}, @invalid_attrs)
    refute changeset.valid?
  end
end
