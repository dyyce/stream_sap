defmodule Twitchsap.LanguageTest do
  use Twitchsap.ModelCase

  alias Twitchsap.Language

  @valid_attrs %{language: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Language.changeset(%Language{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Language.changeset(%Language{}, @invalid_attrs)
    refute changeset.valid?
  end
end
