defmodule Twitchsap.ChannelTest do
  use Twitchsap.ModelCase

  alias Twitchsap.Channel

  @valid_attrs %{game: "some content", language: "some content", name: "some content", type: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Channel.changeset(%Channel{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Channel.changeset(%Channel{}, @invalid_attrs)
    refute changeset.valid?
  end
end
